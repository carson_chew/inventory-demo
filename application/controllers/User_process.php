<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_process extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('user_model');
	}

	// Create user account
	public function new_account(){

		$username = post('username');

		$user_date = array(
			'up_name'         => $username,
			'up_created_date' => today_date()
		);


		if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit_account'])){

			$sql   = $this->user_model->new_user_profile($user_date);
			$up_id = $this->db->insert_id();

			if ($sql == true) {
				log_activity("User successfully registered account. The id is $up_id.");

				// User session
				$user_data_session = array(
					'user_name'             => $username,
					'user_id'               => $up_id,
					'user_logged_in'        => true
				);

				$this->session->set_userdata($user_data_session);

				$this->session->set_flashdata('msg_notification', 107);
				redirect('product_list', 'location', null);
				exit();
			} else {
				$this->session->set_flashdata('msg_notification', 1);
				redirect('register', 'location', null);
				exit();
			}
		}
	}


	// Login Process
	public function login(){

		$username = post('username');

		if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit_login'])){

			$sql = $this->user_model->single_user_profile_by_name($username);

			if ($sql != false) {

				// User session
				$user_data_session = array(
					'user_name'             => $sql->up_name,
					'user_id'               => $sql->up_id,
					'user_logged_in'        => true
				);

				$this->session->set_userdata($user_data_session);

				redirect('product_list', 'location', null);
				exit();
			} else {
				$this->session->set_flashdata('msg_notification', 108);
				redirect('home', 'location', null);
				exit();
			}
		}
	}


	// Logout process
	public function logout(){

		$this->session->sess_destroy();
		redirect('home', 'location', null);
		exit();
	}

}

/* End of file User_process.php */
/* Location: ./application/controllers/User_process.php */
