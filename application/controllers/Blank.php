<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blank extends CI_Controller {

	public function __construct(){
		parent::__construct();
		user_is_logged();
	}

	public function index(){
		$this->load->view('blank_view');
	}
}

/* End of file Blank.php */
/* Location: ./application/controllers/Blank.php */
