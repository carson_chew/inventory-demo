<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_list extends CI_Controller {

	public function __construct(){
		parent::__construct();
		user_is_logged();

		$this->load->model('product_model');
	}

	public function index(){

		// Product list
		$data['all_product'] = $this->product_model->all_product();

		// Total Product List
		$total_product = $this->product_model->count_all_product();

		// Generate page link
		$data['page_links'] = pagination($total_product, 2, "product_list", get("per_page"), null, true, "per_page");

		$this->load->view('product_list_view', $data);
	}

}

/* End of file Product_list.php */
/* Location: ./application/controllers/Product_list.php */
