<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_process extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('category_model');
		$this->load->model('product_model');
	}


	// ==================================================================
	//
	// Category
	//
	// ------------------------------------------------------------------



	// Create category
	public function new_category(){

		$cat_name = post('cat_name');

		$cat_date = array(
			'cat_name' => $cat_name
		);


		if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit_new_category'])){

			$sql    = $this->category_model->new_category_profile($cat_date);
			$cat_id = $this->db->insert_id();

			if ($sql == true) {
				log_activity("User successfully created category. The id is $cat_id.");

				$this->session->set_flashdata('msg_notification', 2);
				redirect('category_list', 'location', null);
				exit();
			} else {
				$this->session->set_flashdata('msg_notification', 1);
				redirect('category_list', 'location', null);
				exit();
			}
		}
	}


	// Edit category
	public function edit_category($cat_id){

		$cat_name = post('cat_name');

		$cat_date = array(
			'cat_name' => $cat_name
		);


		if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit_edit_category'])){

			$sql = $this->category_model->update_category_profile($cat_id, $cat_date);

			if ($sql == true) {
				log_activity("User successfully edited category. The id is $cat_id.");

				$this->session->set_flashdata('msg_notification', 2);
				redirect('category_list', 'location', null);
				exit();
			} else {
				$this->session->set_flashdata('msg_notification', 1);
				redirect('category_list', 'location', null);
				exit();
			}
		}
	}





	// ==================================================================
	//
	// Product
	//
	// ------------------------------------------------------------------


	// Create Product
	public function new_product(){

		$pro_name = post('pro_name');
		$pro_cat  = post('pro_cat');
		$pro_desc = post('pro_desc');

		$product_data = array(
			'pp_name'         => all_char($pro_name),
			'pp_cat_id'       => $pro_cat,
			'pp_desc'         => $pro_desc,
			'pp_created_date' => today_date()
		);


		if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit_new_product'])){

			// Upload file
			if (!empty($_FILES["pro_img"]['tmp_name'])) {
				$pro_img = upload_file("pro_img", "media/product_img/", 7242880, array('jpg', 'jpeg', 'png', 'JPG', 'JPEG', 'PNG', 'pdf'));

				$product_data['pp_img_link'] = $pro_img;
			}

			$sql = $this->product_model->new_product_profile($product_data);
			$pid = $this->db->insert_id();

			if ($sql == true) {
				log_activity("User successfully created product profile. The id is $pid.");

				$this->session->set_flashdata('msg_notification', 2);
				redirect('product_list', 'location', null);
				exit();
			} else {
				$this->session->set_flashdata('msg_notification', 1);
				redirect('product_list', 'location', null);
				exit();
			}
		}
	}


	// Edit product
	public function edit_product($pid){

		$pro_name = post('pro_name');
		$pro_cat  = post('pro_cat');
		$pro_desc = post('pro_desc');

		// Current profile
		$current  = $this->product_model->single_product_profile($pid);
		$old_path = $current->pp_img_link;


		$product_data = array(
			'pp_name'   => all_char($pro_name),
			'pp_cat_id' => $pro_cat,
			'pp_desc'   => $pro_desc
		);


		if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit_edit_product'])){

			// Upload file
			if (!empty($_FILES["pro_img"]['tmp_name'])) {
				$pro_img = upload_file("pro_img", "media/product_img/", 7242880, array('jpg', 'jpeg', 'png', 'JPG', 'JPEG', 'PNG', 'pdf'));

				$product_data['pp_img_link'] = $pro_img;
			}

			$sql = $this->product_model->update_product_profile($pid, $product_data);

			if ($sql == true) {
				log_activity("User successfully edited product. The id is $pid.");

				// Delete product img
				if (!empty($_FILES["pro_img"]['tmp_name'])) {
					delete_file($old_path);
				}

				$this->session->set_flashdata('msg_notification', 2);
				redirect('product_list', 'location', null);
				exit();
			} else {
				$this->session->set_flashdata('msg_notification', 1);
				redirect('product_list', 'location', null);
				exit();
			}
		}
	}


	// Delete product
	public function delete_product($pid){

		// Current profile
		$current  = $this->product_model->single_product_profile($pid);
		$old_path = $current->pp_img_link;

		if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit_delete_product'])){

			$sql = $this->product_model->delete_product_profile($pid);

			if ($sql == true) {
				log_activity("User successfully deleted product. The id is $pid.");

				// Delete product img
				delete_file($old_path);

				$this->session->set_flashdata('msg_notification', 12);
				redirect('product_list', 'location', null);
				exit();
			} else {
				$this->session->set_flashdata('msg_notification', 13);
				redirect('product_list', 'location', null);
				exit();
			}
		}
	}

}

/* End of file Product_process.php */
/* Location: ./application/controllers/Product_process.php */
