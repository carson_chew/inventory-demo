<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_list extends CI_Controller {

	public function __construct(){
		parent::__construct();
		user_is_logged();

		$this->load->model('category_model');
	}

	public function index(){

		// Show all category
		$data['all_category'] = $this->category_model->all_category();

		$this->load->view('category_list_view', $data);
	}

}

/* End of file Category_list.php */
/* Location: ./application/controllers/Category_list.php */
