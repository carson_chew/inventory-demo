<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once FCPATH. 'application/third_party/vendor/tecnickcom/tcpdf/tcpdf.php';

class Pdf extends TCPDF {

    function __construct(){
        parent::__construct();
    }

}

/* End of file Pdf.php */
/* Location: ./application/libraries/Pdf.php */
