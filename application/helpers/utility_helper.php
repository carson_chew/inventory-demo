<?php
defined('BASEPATH') OR exit('No direct script access allowed');



// ==================================================================
//
// Short form
//
// ------------------------------------------------------------------


/**
 * Short form of CI Instance
 */
if (! function_exists('ci')){

	function ci(){
		return $ci=& get_instance();
	}
}


/**
 * Short form of post
 */
if (! function_exists('post')){

	function post($data, $xss = TRUE){

		$ci = ci();

		$post_result = $ci->input->post($data, $xss);

		if (empty($post_result)) {
			return $post_result;
		} else {
			return is_array($post_result) ? $post_result : trim($post_result);
		}
	}
}



/**
 * Short form of get
 */
if (! function_exists('get')){

	function get($data, $xss = TRUE){

		$ci = ci();

		$get_result = $ci->input->get($data, $xss);

		return trim($get_result);
	}
}


/**
 * Debug the post data
 */
if (! function_exists('debug_post')){

	function debug_post(){

		echo '<pre>';
		print_r($_POST);
		echo '</pre>';
	}
}


/**
 * Debug the get data
 */
if (! function_exists('debug_get')){

	function debug_get(){

		echo '<pre>';
		print_r($_GET);
		echo '</pre>';
	}
}


/**
 * Debug the array
 */
if (! function_exists('printr')){

	function printr($array){

		echo '<pre>';
		print_r($array);
		echo '</pre>';
	}
}


/**
 * Debug the session data
 */
if (! function_exists('session')){

	function session(){

		foreach ($_SESSION as $name => $value){
			echo $name."=".$value."<br>";
		}
	}
}



/**
 * Short form for decode html
 */
function html_decode($foo){
	return htmlspecialchars_decode(html_entity_decode($foo));
}


/**
 * For show * as required field
 */
function required_star(){
	return '<span style="color: red;" data-toggle="tooltip" title="Maklumat ini wajib diisi">*</span>';
}

// ==================================================================
//
// Message handler
//
// ------------------------------------------------------------------

//Get message UI
function message_handler_js($msg_id){

	$ci = ci();

	$array_message = $ci->load->message_handler_model->get_msg_handler($msg_id);

	if ($array_message == false) {
		echo '<div class="alert alert-danger alert-dismissible">
	            <h5><i class="icon fas fa-ban"></i> Message</h5>
	            Oops! Something was wrong. Please contact administrator for further help.
	          </div>';
	}


	if ($array_message['msg_type'] == 'pass') {
		echo '<div class="alert alert-success alert-dismissible">
	            <h5><i class="icon fas fa-check"></i> Message</h5>
	            '.$array_message['msg_desc'].'
	          </div>';
	} else {
		echo '<div class="alert alert-danger alert-dismissible">
	            <h5><i class="icon fas fa-ban"></i> Message</h5>
	            '.$array_message['msg_desc'].'
	          </div>';
	}
}

//Get message UI - Customize
function customize_message_handler($custom_message_notif){

	if ($custom_message_notif['cus_msg_type'] == 'pass') {
		echo '<div class="alert alert-success alert-dismissible">
	            <h5><i class="icon fas fa-check"></i> Message</h5>
	            '.$custom_message_notif['cus_msg_desc'].'
	          </div>';
	} else {
		echo '<div class="alert alert-danger alert-dismissible">
	            <h5><i class="icon fas fa-ban"></i> Message</h5>
	            '.$custom_message_notif['cus_msg_desc'].'
	          </div>';
	}
}


//Display message
function error_msg(){

	$ci= ci();
	$message_notif = $ci->session->flashdata('msg_notification');

	//For single error and the error msg in db
	if (isset($message_notif) && !empty($message_notif)) {
		message_handler_js($message_notif);
	}

	$custom_message_notif = $ci->session->flashdata('customize_msg_notification');

	//For msg that not inside the db
	if (isset($custom_message_notif) && !empty($custom_message_notif)) {
		customize_message_handler($custom_message_notif);
	}
}


/**
 * Get error message without ui
 */
function no_ui_message($msg_id){

	$ci = ci();

	$message = $ci->load->message_handler_model->get_raw_msg($msg_id);

	if($message != false){
		return $message;
	}
}



// ==================================================================
//
// Log activity function
//
// ------------------------------------------------------------------
function log_activity($activity_description){

	$ci = ci();

	$ci->load->library('session');
	$ci->load->helper('security');


	$user_ip            = ($_SERVER['REMOTE_ADDR'] == "::1") ? "127.0.0.1" : $_SERVER['REMOTE_ADDR'];
	$req_url            = $_SERVER['REQUEST_URI'];
	$user_id            = $ci->session->userdata("user_id");
	$user_agent         = $_SERVER['HTTP_USER_AGENT'];
	$today_date         = date ("Y-m-d H:i:s");
	$user_computer_name = gethostname();


	// Get IP address
	if(empty($user_ip)) {
		$user_ip = "Unable To Get User/Remote IP Address.";
	}

	// Get requested script
	if(empty($req_url)) {
		$req_url = "Unable To Get REQUEST URL.";
	}

	//Get computer name
	if (empty($user_computer_name)) {
		$user_computer_name = "Unable To Get User Computer Name.";
	}

	$sql_array = array(
		'al_user_id'       => $ci->security->xss_clean($user_id),
		'al_activity'   => $ci->security->xss_clean($activity_description),
		'al_req_url'       => $ci->security->xss_clean($req_url),
		'al_remote_ip'     => $ci->security->xss_clean($user_ip),
		'al_user_info' => $ci->security->xss_clean($user_agent." Device Name: ".$user_computer_name),
		'al_created_date'  => $ci->security->xss_clean($today_date)
	);

	$sql = $ci->db->insert('activity_log', $sql_array);

	if($sql == FALSE) {
		echo ('<script language="JavaScript">alert("Please contact system administrator. Sorry for inconvenience.");</script>');
		echo '<script>if (window.opener && !window.opener.closed) {
			window.opener.location.reload();  //this line reloads the opener-parent window
			window.close(); //this line closes the pop-up window
		}</script>';
		echo '<meta http-equiv="refresh" content="0;URL=index.php">';
	}

}






// ==================================================================
//
// Password MISC
//
// ------------------------------------------------------------------


/**
 * Implement random number and hash it using bcrypt
 * Normally use for password
 */
function hash_random_number($password){

	$hash     = password_hash($password, PASSWORD_BCRYPT);

	$result = array('raw_password' => $password, 'hashed_password' => $hash);

	return $result;
}



/**
 * User password key
 */
function hash_key($raw_password){

	$hash = password_hash($raw_password, PASSWORD_BCRYPT, array("cost" => 11));

	return $hash;
}



/**
 * Generate random password
 */
function random_password($length, $count, $characters) {

	// $length - the length of the generated password
	// $count - number of passwords to be generated
	// $characters - types of characters to be used in the password

	// define variables used within the function
	$symbols      = array();
	$passwords    = array();
	$used_symbols = '';
	$pass         = '';

	// an array of different character types
	$symbols["lower_case"]      = 'abcdefghijklmnopqrstuvwxyz';
	$symbols["upper_case"]      = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$symbols["numbers"]         = '1234567890';
	$symbols["special_symbols"] = '!?~@#-_+<>[]{}';

    $characters = explode(",",$characters); // get characters types to be used for the passsword

    foreach ($characters as $key=>$value) {
        $used_symbols .= $symbols[$value]; // build a string with all characters
    }

    $symbols_length = strlen($used_symbols) - 1; //strlen starts from 0 so to get number of characters deduct 1

    for ($p = 0; $p < $count; $p++) {
        $pass = '';

        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $symbols_length); // get a random character from the string with all characters
            $pass .= $used_symbols[$n]; // add the character to the password string
        }

        $passwords[] = $pass;
    }

    return $passwords; // return the generated password
}










// ==================================================================
//
// Check User Credential
//
// ------------------------------------------------------------------


/**
 * Check user session
 * $check_account = 1 (Default with check the account completion status), 2 = Exclude checking
 */
function user_is_logged(){

	$ci = ci();

	$log_status = $ci->session->userdata("user_logged_in");

	if ($log_status != true) {
		$ci->session->set_flashdata('msg_notification', 109);
		redirect('home', 'location', null);
		exit();
	}
}


//Get current user id
function user_id(){

	$ci= ci();

	$user_id = $ci->session->userdata("user_id");

	return $user_id;
}



//Check admin user
function is_admin(){

	$ci= ci();

	$user_id = $ci->session->userdata("user_id");

	$allowed_user = array(12);

	$result = (in_array($user_id, $allowed_user)) ? true : false ;

	if ($result == false) {
		$ci->session->set_flashdata('msg_notification', 43);
		redirect('app/dashboard', 'location', null);
		exit();
	}
}




// ==================================================================
//
// MISC
//
// ------------------------------------------------------------------


/**
 * Implement verify the email
 */
function email_sanitize($email) {

    // Remove all illegal characters from email
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);

    // Validate e-mail
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return $email;
    } else {
        return false;
    }
}



// Use to check null value
function check_empty($value, $msg = "N/A"){

	if (empty($value) || $value == "" || $value == '0' || $value == false || $value == null) {
		$result = $msg;
	} else {
		$result = $value;
	}

	return $result;
}


/**
 * return value only
 */
function return_value($db_name, $column_name, $display_value, $variable_id, $vocabulary_category = null, $msg = ""){

	$ci = ci();

	if ($vocabulary_category != null) {
		$ci->db->where($vocabulary_category);
	}

	$ci->db->where($column_name, $variable_id);
	$sql = $ci->db->get($db_name, 1, null);

	if ($sql->num_rows() == 0) {
		return $msg;
	} else {
		$result = $sql->row();

		return $result->$display_value;
	}
}


/**
 *  count vocab rows by category
 */
function count_vocab($vocab_category) {

	$ci = ci();

	$ci->db->where('v_category', $vocab_category);
	$sql = $ci->db->count_all_results('vocabulary');

	return $sql;
}



/**
*  get vocab rows by category
*/
function get_vocab_rows($vocab_category) {

	$ci = ci();

	$ci->db->where('v_category', $vocab_category);
	$sql = $ci->db->get('vocabulary', null, null);

	if ($sql->num_rows() != 0) {
		return $sql->result();
	} else {
		return false;
	}
}



/**
 * Split array into text
 */
function split_array_to_text($array_set, $db_name, $con_field, $field_value, $vocabulary_category = "") {

	if (!empty($array_set)) {
		$result = explode(',', $array_set);

		foreach ($result as $key => $value) {
			$r = return_value($db_name, $con_field, $field_value, $value, $vocabulary_category = "");
			$new_value[] = $r;
		}

		return implode(', ',$new_value);
	}else{
		return "N/A";
	}
}



/**
 * Combine array and make it unique array
 */
function combine_unique_array($array_one, $array_two, $convert_array_one = null, $convert_array_two = null) {

	// Convert array one to array
	if (!empty($array_one) && $convert_array_one != null) {
		$array_one = explode(',', $array_one);
	}

	// Convert array two to array
	if (!empty($array_two) && $convert_array_two != null) {
		$array_two = explode(',', $array_two);
	}

	if (!empty($array_one) && !empty($array_two)) {

		$combined_array = array_merge($array_one, $array_two);

		$clean_array = array_unique($combined_array);

		return $clean_array;
	}else{
		return null;
	}
}


// Redirect to page
function redirect_to_previous($redirect_path = null){

	$ci = ci();

	$ci->load->library('user_agent');


	if ($redirect_path == null) {

		if ($ci->agent->is_referral()){
			$last_page =  $ci->agent->referrer();
		}else{
			$last_page = "app/dashboard";
		}

	} else {
		$last_page = $redirect_path;
	}


	$ci->session->set_flashdata('msg_notification', 43);
	redirect($last_page, 'location', null);
	exit();
}



// Random pick from array
function random_array_pick($array, $number){

	$input = $array;
	$rand_keys = array_rand($input, $number);

	return $input[$rand_keys[0]];
}


// Delete value from array
function delete_array_value($full_array, $keyword){

	if (!empty($full_array)) {
		foreach (array_keys($full_array, $keyword) as $key) {
			unset($full_array[$key]);
		}

		return $full_array;
	}else{
		return $full_array;
	}
}


// use to get current page url
function current_url_with_param($tab_id = null){

	$param = (empty($_SERVER['QUERY_STRING'])) ? null : '?'.$_SERVER['QUERY_STRING'] ;

	return current_url().$param.$tab_id;
}

// ==================================================================
//
// Character setting
//
// ------------------------------------------------------------------

/**
 * Implement first char uppercase
 */
function fchar($char){
	return ucwords($char);
}

/**
 * Implement all upper case
 */
function all_char($foo){
	return strtoupper($foo);
}


/**
 * Split the array with comma
 */
if ( ! function_exists('split_array_comma')){

	function split_array_comma($array_set, $spliter = ","){

		if (!empty($array_set)) {
			$result = implode($spliter, $array_set);
		} else {
			$result = null;
		}

		return $result;
	}
}


/**
 * Implement trim description
 */
function trim_desc($string, $length = 50, $trimmarker = '...') {
	$len = strlen(trim($string));
	$newstring = ($len > $length) ? rtrim(substr($string, 0, strrpos(substr($string, 0, $length), ' '))) . $trimmarker : $string;

	return $newstring;
}


/**
 * Implement trim words
 */
function trim_word($text, $words = 10) {

	// Check if string has more than X words
    if (str_word_count($text) > $words) {

        // Extract first X words from string
        preg_match("/(?:[^\s,\.;\?\!]+(?:[\s,\.;\?\!]+|$)){0,$words}/", $text, $matches);
        $text = trim($matches[0]);

        // Let's check if it ends in a comma or a dot.
        if (substr($text, -1) == ',') {
            // If it's a comma, let's remove it and add a ellipsis
            $text = rtrim($text, ',');
            $text .= '...';
        } else if (substr($text, -1) == '.') {
            // If it's a dot, let's remove it and add a ellipsis (optional)
            $text = rtrim($text, '.');
            $text .= '...';
        } else {
            // Doesn't end in dot or comma, just adding ellipsis here
            $text .= '...';
        }
    }

    // Returns "ellipsed" text, or just the string, if it's less than X words wide.
    return $text;
}




// Remove string after found some word
function remove_string_after_found($string, $founded_string = ":"){

	$result = preg_replace("/$founded_string.*/", "", $string);

	return $result;
}


// Get portion of words by symbol
function get_portion_words($string, $founded_string = ":", $position = 4){

	$half_clean = substr($string, strrpos($string, $founded_string) +1);

	$result = mb_substr($half_clean, 0, $position);

	return $result;
}


// ==================================================================
//
// File MISC
//
// ------------------------------------------------------------------


/**
 * Check file path
 */
function verify_file_path($path, $on_link = null, $download_text = "Download", $class = null, $return_msg = "N/A") {

	if (!empty($path) && file_exists($path)) {

		$valid_path = base_url().$path;

		if ($on_link != null) {
			return '<a href="'.$valid_path.'" target="_new" '.$class.'>'.$download_text.'</a>';
		} else {
			return $valid_path;
		}

	} else {
		return $return_msg;
	}
}



/**
 * Verify file path and show empty image
 */
function verify_img_exist($path, $default_img = "bootstrap/img/blank.jpg") {

	if (!empty($path) && file_exists($path)) {

		$valid_path = base_url().$path;

		return $valid_path;

	} else {
		return base_url().$default_img;
	}
}


/**
  * $max_file_size = 900000 byte
  */
function upload_file($filename, $real_folder_path, $max_file_size = 900000, $allow_ext = array('gif', 'jpg', 'jpeg', 'png')) {

	$ci = ci();

	$Upload = new \Rundiz\Upload\Upload($filename);

    $Upload->move_uploaded_to = $real_folder_path;

    // Allowed for gif, jpg, png
    $Upload->allowed_file_extensions = $allow_ext;

    // Max file size is 900KB.
    $Upload->max_file_size = $max_file_size;

    // You can name the uploaded file to new name or leave this to use its default name. Do not included extension into it.
    $Upload->new_file_name = uniqid();

    // Overwrite existing file? true = yes, false = no
    $Upload->overwrite = false;

    // Web safe file name is English, number, dash, underscore.
    $Upload->web_safe_file_name = true;

    // Scan for embedded php or perl language?
    $Upload->security_scan = true;

    // If you upload multiple files, do you want it to be stopped if error occur? (Set to false will skip the error files).
    $Upload->stop_on_failed_upload_multiple = false;

    // Begins upload
    $upload_result = $Upload->upload();

    // Get the uploaded file's data.
    $uploaded_data = $Upload->getUploadedData();


    if ($upload_result == true) {

    	// Db path
        if (count($uploaded_data) == 1) {
        	return $real_folder_path.$uploaded_data[0]['new_name'];
        } elseif (count($uploaded_data) > 1) {

        	foreach ($uploaded_data as $key => $files_detail) {
        		$new_result[] = $real_folder_path.$files_detail['new_name'];
        	}

        	return $new_result;
        }

    } else {

    	// To check for the errors.
	    if (is_array($Upload->error_messages) && !empty($Upload->error_messages)) {

	        foreach ($Upload->error_messages as $error_message) {
	            $ci->session->set_flashdata('customize_msg_notification', array('cus_msg_type' => 'failed', 'cus_msg_desc' => $error_message));
	        }
	    }

	    return false;
    }
}


/**
 * Check file path, then delete file
 */
function delete_file($path) {

	if (!empty($path) && file_exists($path)) {
		unlink($path);
	}
}



/**
 * Implement get current filename that without parameter
 */
function getFileName(){

	$link = $_SERVER['HTTP_REFERER'];
	$base_filename = basename($link);

	if(substr($base_filename,0,strrpos($base_filename,'?')) == true){
		$file = substr($base_filename,0,strrpos($base_filename,'?'));
	}else{
		$file = basename($link);
	}

	return $file;
}



/**
 * Get current file extension
 */
function extension_checker($link, $allow_ext_array){

	$file_data = pathinfo($link, PATHINFO_EXTENSION);

	if (in_array($file_data, $allow_ext_array) == true) {
		return true;
	}else{
		return false;
	}
}



/**
 * Implement css includer
 */
function css_includer($foo_list = null){

	if ($foo_list != null) {
		foreach ($foo_list as $link) {
			echo '<link href="'.base_url().$link.'" rel="stylesheet">'.PHP_EOL;
		}
	}
}


/**
 * Implement js includer
 */
function js_includer($foo_list = null){

	if ($foo_list != null) {
		foreach ($foo_list as $link) {
			echo '<script type="text/javascript" src="'.base_url().$link.'"></script>'.PHP_EOL;
		}
	}
}


// Combine array
function combine_three_array($sub_ques, $scheme, $mark) {
    $result = array();

    foreach ( $sub_ques as $key => $ques ) {
        $result[] = array( 'sub_question' => $ques, 'scheme' => $scheme[$key], 'mark' => $mark[$key] );
    }

    return $result;
}



