<?php $this->load->view('header'); ?>

  <div class="content-wrapper">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2"></div>
      </div>
    </div>

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">

          <!-- Put Your Content Here -->
          <div class="col-lg-12">
            <?php error_msg(); ?>
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="m-0">Category List</h5>
              </div>
              <div class="card-body">

                <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newCategory">New</button></p>

                <?php if ($all_category == false): ?>
                  <p>No Data Found</p>
                <?php else: ?>
                  <div class="table-responsive">
                    <table class="table table-hover table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Category</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($all_category as $akey => $row): ?>
                          <tr>
                            <td><?= $akey+1; ?></td>
                            <td><?= $row->cat_name; ?></td>
                            <td>
                              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#editCat<?= $akey+1; ?>">
                                <i class="fa fa-edit"></i>
                              </button>

                              <!-- Modal -->
                              <div class="modal fade" id="editCat<?= $akey+1; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="<?= site_url('product_process/edit_category/'.$row->cat_id); ?>" method="POST">
                                      <div class="modal-body">
                                        <div class="form-group">
                                          <label for="cat_name">Category</label>
                                          <input type="text" class="form-control" id="cat_name" name="cat_name" required value="<?= $row->cat_name; ?>">
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" name="submit_edit_category" class="btn btn-primary">Save Changes</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </td>
                          </tr>
                        <?php endforeach ?>
                      </tbody>
                    </table>
                  </div>
                <?php endif ?>


              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

  </div>


<!-- New Category -->
<div class="modal fade" id="newCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= site_url('product_process/new_category'); ?>" method="POST">
        <div class="modal-body">

          <div class="form-group">
            <label for="cat_name">Category</label>
            <input type="text" class="form-control" id="cat_name" name="cat_name" required>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" name="submit_new_category" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>
