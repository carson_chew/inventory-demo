<?php $this->load->view('header'); ?>

  <div class="content-wrapper">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2"></div>
      </div>
    </div>

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">

          <!-- Put Your Content Here -->
          <div class="col-lg-12">
            <?php error_msg(); ?>

            <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="m-0">Product List</h5>
              </div>
              <div class="card-body">

                <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newProduct">New</button></p>

                <?php if ($all_product == false): ?>
                  <p>No Data Found</p>
                <?php else: ?>
                  <div class="table-responsive">
                    <table class="table table-hover table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Image</th>
                          <th>Product Name</th>
                          <th>Category</th>
                          <th>Description</th>
                          <th style="width: 15%;">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $xx = get("per_page") + 1; ?>
                        <?php foreach ($all_product as $pKey => $product): ?>
                          <?php $image_url = verify_img_exist($product->pp_img_link, "media/product_img/blank.png"); ?>
                          <tr>
                            <td><?= $pKey+$xx; ?></td>
                            <td><img src="<?= $image_url; ?>" alt="Product" style="height: 50px;"></td>
                            <td><?= $product->pp_name; ?></td>
                            <td><?= $product->cat_name; ?></td>
                            <td><?= (empty($product->pp_desc)) ? "N/A" : trim_desc($product->pp_desc, 30); ?></td>
                            <td>
                              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#editProduct<?= $pKey+1; ?>"><i class="fa fa-edit"></i></button>

                              <!-- Edit Modal -->
                              <div class="modal fade" id="editProduct<?= $pKey+1; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="<?= site_url('product_process/edit_product/'.$product->pp_id); ?>" method="POST" enctype="multipart/form-data">
                                      <div class="modal-body">

                                        <div class="form-group">
                                          <label for="pro_name">Product Name</label>
                                          <input type="text" class="form-control" id="pro_name" name="pro_name" required value="<?= $product->pp_name; ?>">
                                        </div>

                                        <div class="form-group">
                                          <label for="pro_cat">Category</label>
                                          <select name="pro_cat" id="pro_cat" class="form-control">
                                            <?= $this->dynamic_ui->edit_select_list("category", "cat_id", $product->pp_cat_id, "cat_name", null, null, null); ?>
                                          </select>
                                        </div>

                                        <div class="form-group">
                                          <label for="pro_img">Image</label>
                                          <input type="file" class="form-control" id="pro_img" name="pro_img">
                                        </div>

                                        <div class="form-group">
                                          <label for="pro_desc">Description</label>
                                          <textarea class="form-control" name="pro_desc" id="pro_desc" rows="4"><?= $product->pp_desc; ?></textarea>
                                        </div>

                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" name="submit_edit_product" class="btn btn-primary">Save Changes</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delProduct<?= $pKey+1; ?>"><i class="fas fa-trash"></i></button>

                              <!-- Delete Modal -->
                              <div class="modal fade" id="delProduct<?= $pKey+1; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="<?= site_url('product_process/delete_product/'.$product->pp_id); ?>" method="POST">
                                      <div class="modal-body">
                                        <p>Are you sure want to delete this record? Once delete it cannot be undone.</p>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" name="submit_delete_product" class="btn btn-danger">Delete</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                            </td>
                          </tr>
                        <?php endforeach ?>
                      </tbody>
                    </table>
                  </div>

                  <?php if (!empty($page_links)): ?>
                    <p><?= $page_links; ?></p><br>
                  <?php endif ?>
                <?php endif ?>

              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

  </div>


  <!-- New Product -->
  <div class="modal fade" id="newProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">New</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="<?= site_url('product_process/new_product'); ?>" method="POST" enctype="multipart/form-data">
          <div class="modal-body">

            <div class="form-group">
              <label for="pro_name">Product Name</label>
              <input type="text" class="form-control" id="pro_name" name="pro_name" required>
            </div>

            <div class="form-group">
              <label for="pro_cat">Category</label>
              <select name="pro_cat" id="pro_cat" class="form-control">
                <option value="">Select Category</option>
                <?= $this->dynamic_ui->load_all_select_list("category", "cat_name", "cat_id", null, null, null, null, null, " - "); ?>
              </select>
            </div>

            <div class="form-group">
              <label for="pro_img">Image</label>
              <input type="file" class="form-control" id="pro_img" name="pro_img">
            </div>

            <div class="form-group">
              <label for="pro_desc">Description</label>
              <textarea class="form-control" name="pro_desc" id="pro_desc" rows="4"></textarea>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="submit_new_product" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>

<?php $this->load->view('footer'); ?>

