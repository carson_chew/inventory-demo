<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Register</title>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="<?= site_url('bootstrap/plugins/fontawesome-free/css/all.min.css'); ?>">
  <link rel="stylesheet" href="<?= site_url('bootstrap/plugins/icheck-bootstrap/icheck-bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="<?= site_url('bootstrap/dist/css/adminlte.min.css'); ?>">
</head>
<body class="hold-transition register-page">

  <div class="register-box">

    <?= error_msg(); ?>

    <div class="card card-outline card-primary">
      <div class="card-header text-center">
        <span class="h1">Inventory Web App</span>
      </div>
      <div class="card-body">
        <p class="login-box-msg">Register a new membership</p>

        <form action="<?= site_url('user_process/new_account'); ?>" method="post">
          <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="Full name" name="username" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>

          <button type="submit" name="submit_account" class="btn btn-primary btn-block">Register</button>
        </form>

        <a href="<?= site_url('home'); ?>" class="text-center">I already have a membership</a>
      </div>
    </div>
  </div>

  <script src="<?= site_url('bootstrap/plugins/jquery/jquery.min.js'); ?>"></script>
  <script src="<?= site_url('bootstrap/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
  <script src="<?= site_url('bootstrap/dist/js/adminlte.min.js'); ?>"></script>
</body>
</html>
