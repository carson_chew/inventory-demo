<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ESLDN TOT</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="<?= site_url('bootstrap/plugins/fontawesome-free/css/all.min.css'); ?>">
  <link rel="stylesheet" href="<?= site_url('bootstrap/dist/css/adminlte.min.css'); ?>">
</head>
<body class="hold-transition sidebar-mini">
  <div class="wrapper">

    <nav class="main-header navbar navbar-expand navbar-dark navbar-danger">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
      </ul>
    </nav>


    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-light-danger elevation-4">

      <!-- Logo -->
      <a href="" class="brand-link navbar-danger">
        <img src="<?= site_url('bootstrap/dist/img/AdminLTELogo.png'); ?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Inventory Web App</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="<?= site_url('bootstrap/dist/img/user2-160x160.jpg'); ?>" class="img-circle elevation-2" alt="User Image">
          </div>
          <div class="info">
            <a href="#" class="d-block"><?= $this->session->userdata("user_name"); ?></a>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

            <li class="nav-item">
              <a href="<?= site_url('blank'); ?>" class="nav-link">
                <i class="nav-icon fas fa-th"></i> <p>Sample Page</p>
              </a>
            </li>

            <li class="nav-item">
              <a href="<?= site_url('category_list'); ?>" class="nav-link">
                <i class="nav-icon fas fa-asterisk"></i> <p>Category</p>
              </a>
            </li>

            <li class="nav-item">
              <a href="<?= site_url('product_list'); ?>" class="nav-link">
                <i class="nav-icon fas fa-database"></i> <p>Product</p>
              </a>
            </li>

            <li class="nav-item">
              <a href="<?= site_url('user_process/logout'); ?>" class="nav-link">
                <i class="nav-icon fas fa-sign-out-alt"></i> <p>Logout</p>
              </a>
            </li>

          </ul>
        </nav>

      </div>
    </aside>
